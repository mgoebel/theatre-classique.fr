<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="3.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    <xsl:mode on-no-match="shallow-copy" />

    <xsl:output encoding="UTF-8" indent="yes" method="xml" media-type="application/xml"/>

    <xsl:template match="tei:author">
        <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="author">
            <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="persName">
                <xsl:value-of select="."/>
            </xsl:element>
            <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="idno">
                <xsl:attribute name="type" select="'ISNI'"/>
                <xsl:attribute name="xml:base" select="'http://isni.org/isni/'"/>
                <xsl:value-of select="@ISNI => replace(' ', '')"/>
            </xsl:element>
        </xsl:element>

        <xsl:if test="./root()//tei:publicationStmt//tei:editor">
            <xsl:copy-of select="./root()//tei:publicationStmt//tei:editor"/>
        </xsl:if>

    </xsl:template>
    <xsl:template match="tei:publicationStmt/tei:editor"/>

    <xsl:template match="tei:rights">
        <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="availability">
            <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="licence">
                <xsl:attribute name="target"
                    select="'https://creativecommons.org/licenses/by-nc-sa/4.0/'"/>
                <xsl:value-of select="."/>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:teiHeader">
        <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="teiHeader">
            <xsl:apply-templates/>

            <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="xenoData">
                <xsl:element namespace="http://theatre-classique.fr" name="sourceDesc">
                    <xsl:for-each select="./root()//tei:sourceDesc/*">
                        <xsl:element namespace="http://theatre-classique.fr" name="{./local-name()}">
                            <xsl:value-of select="."/>
                        </xsl:element>
                    </xsl:for-each>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:fileDesc">
        <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="fileDesc">
            <xsl:apply-templates/>
            <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="sourceDesc">
                <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="bibl">
                    <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="ref">
                        <xsl:value-of select="//tei:sourceDesc/tei:permalien"/>
                    </xsl:element>
                </xsl:element>
            </xsl:element>
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:sourceDesc"/>

    <xsl:template match="tei:l/@id|tei:p/@id|tei:s/@id">
        <xsl:attribute name="n" select="."/>
    </xsl:template>
    
    <xsl:template match="tei:div1|tei:div2">
        <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="div">
            <xsl:copy-of select="@*"/>
            <xsl:attribute name="n" select="local-name(.) => substring(4,1)" />
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="tei:sp[@stage]">
        <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="stage">
            <xsl:attribute name="resp" select="'editor'"/>
            <xsl:value-of select="@stage"/>
        </xsl:element>
        <xsl:element namespace="http://www.tei-c.org/ns/1.0" name="sp">
            <xsl:copy-of select="@*[not( local-name() = 'stage' )]"/>
            <xsl:copy-of select="./tei:speaker"/>
            <xsl:apply-templates />    
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:sp/@stage"/>
    
</xsl:stylesheet>

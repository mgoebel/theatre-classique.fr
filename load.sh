#!/bin/bash
source functions.sh

POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -e|--exist)
    exist="$2"
    shift # past argument
    shift # past value
    ;;
    -f|--file)
    file="$2"
    shift # past argument
    shift # past value
    ;;
    --default)
    DEFAULT=YES
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done

# check for exist available
while [ $(curl --head --silent "http://${exist}" | grep -c "200 OK") == 0 ];
do
    echo -ne "\rhttp://${exist}" is not available.
    sleep 2s
done

# upload to exist
curl -u admin: -T ${file} "http://${exist}/exist/rest/db/"
# execute
curl -u admin: --output load.txt "http://${exist}/exist/rest/db/?_query=util:eval(xs:anyURI('${file}'))"

# parse
cat load.txt | grep --only-matching --extended-regexp "/db.+\.xml" > list.txt

get-from-db

exit 0